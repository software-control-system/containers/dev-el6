FROM centos:6

RUN rm -rf /etc/localtime && ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime \
    && sed -i '/^mirrorlist/s/^/#/;/^#baseurl/{s/#//;s/mirror.centos.org\/centos\/$releasever/vault.centos.org\/6.10/}' /etc/yum.repos.d/*B* \
    && yum -y update && yum clean all \
    && yum -y groupinstall -y "Development Tools" \
    && yum -y install cmake \
    && yum -y install epel-release \
    && yum -y install cmake3

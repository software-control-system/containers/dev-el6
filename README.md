# dev-el6

Docker container image based on official CentOS 6 image with development tools

- gcc version 4.4.7 20120313 (Red Hat 4.4.7-23) (GCC)
- cmake version 2.8.12.2
- cmake3 version 3.6.1

## Build image

```
docker build -t registry.gitlab.com/synchrotron-soleil/control-system/containers/dev-el6 .
```

If proxy issue

```
docker build --build-arg http_proxy=http://proxy.synchrotron-soleil.fr:8080 --build-arg https_proxy=http://proxy.synchrotron-soleil.fr:8080 -t registry.gitlab.com/synchrotron-soleil/control-system/containers/dev-el6 .
```

## Example of usage

```
$ git clone git@gitlab.synchrotron-soleil.fr:control-system/sandbox/cmake-catch1-hello.git
...
$ docker run --rm -it --user $UID -v $(pwd):/data registry.gitlab.com/synchrotron-soleil/control-system/containers/dev-el6
bash-4.2$ cd /data
bash-4.2$ cmake3 -H. -Bbuild
...
bash-4.2$ cmake3 --build build
...
bash-4.2$ build/unit_tests
...
bash-4.2$ build/unit_tests -r xml
...
bash-4.2$ build/SayHello
Hello World
```
